s4
==

Lighweight & robust file uploader


* __Authors__: [Ondrej Sika](http://ondrejsika.com/c.html)
* __GitHub__: <https://github.com/ondrejsika/s4>
* __Python Package Index__: <https://pypi.python.org/pypi/s4>


Documentation
-------------

### Installation

```
pip install s4
```

### Configuration


Create `wsgi.py`

``` python
from s4 import get_wsgi

DRIVES = ["a", "b", "c"]
STATIC_ROOT = "/var/s4"  # no endslash required
URL_PREFIX = "http://static.example.com/"  # endslash required

application = get_wsgi(static_root=STATIC_ROOT, static_url=URL_PREFIX, drives=DRIVES)
```


### Run

Start via gunicorn

```
gunicorn wsgi:application -b 127.0.0.1:9988
```


### Upload file via curl

```
sika@thinkpad:~$ curl -F file=@FILE http://127.0.0.1:9988
http://static.example.com/c/4o6/gxw/2s1/5eayavkm_FILE
```

